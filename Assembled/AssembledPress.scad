include <../variables.scad>




// BASE
translate([0,thickness]){
	// Printing plate
	translate([0,thickness])
%	linear_extrude(thickness){
		square([plateLength,plateWidth]);
	}

	// Right piece
	extrudedRectPieceWithDonutBase();
	translate([0,thickness])
		extrudedRectPieceBase();

	// Left piece
	translate([0,plateWidth+thickness]){
		translate([0,thickness])
			extrudedRectPieceBase();
		translate([0,2*thickness])
			extrudedRectPieceWithDonutBase();
	}
}

// TOP

// LEG
translate([baseLength-radDonut,thickness*2,thickness+radDonut]){
	armA = -180 + atan2(h,l) + a;
	// First leg
	rotate([0,armA,0])
	extrudedLeg(r,r);

	// Second leg
	translate([0,plateWidth+thickness]){
		rotate([0,armA,0])
		extrudedLeg(r,r);
	}
}

// TOP PLATE
translate([xtop,0,ztop])
translate([0,thickness,thickness]){
	//Plate
	translate([0,thickness*2])
%	linear_extrude(thickness){
		square([plateLength,topWidth]);
	}

	translate([0,thickness*2]){
		// Right piece
		extrudedRectPieceWithDonutTop();
	}

	translate([0,thickness*3 + topWidth]){
		// Right piece
		extrudedRectPieceWithDonutTop();
	}
}


// MODULES

module extrudedRectPieceWithDonutTop(){
	rotate([90,0,0])
	linear_extrude(thickness){
		rectPieceWithDonut(thickness,plateLength,plateLength/2);
	}
}


module extrudedRectPieceBase(){
	rotate([90,0,0])
	linear_extrude(thickness){
		rectPiece(thickness,baseLength);
	}
}

module extrudedRectPieceWithDonutBase(){
	rotate([90,0,0])
	linear_extrude(thickness){
		rectPieceWithDonut(thickness,baseLength,baseLength-radDonut);
	}
}

module rectPiece(h,l){
	square([l,h]);
}



// Rectangular piece with a donutWithBase in a given position 
// h is height of the rectangle
// l is length of the rectangle
// ld is the position of the donutWithBase
// rDonut is the radius of the donut
// hole_d is the diameter of the hole
module rectPieceWithDonut(h,l,ld,rDonut=radDonut,hole_d=hole_diam){
	square([l,h]);
	translate([ld,h+rDonut]){
		donutWithBase(rDonut,hole_d);
	}
}

// Centered in the center of the hole
// Radius of the circle
// Diameter of the hole
module donutWithBase(radius,hole_d){
	diam = radius*2;
	difference(){
		union(){
			circle(d=diam);
			translate([-radius,-radius]){
				square([diam,radius]);
			}
		}
		circle(d=hole_d);

	}
}

module extrudedLeg(l1,l2){
	rotate([90,0,0])
	linear_extrude(thickness){
		legWithThreeDonuts(l1,l2,thickness);
	}
}

module legWithThreeDonuts(l1,l2,h,rDonut=radDonut,hole_d=hole_diam){
	hole_r = hole_d/2;
	donut(rDonut,hole_d);
	translate([hole_r,-h/2]){
		square([l1-rDonut,h]);

		translate([l1,0]){
			square([l2-rDonut,h]);
		}
	}

	// Donuts
	translate([l1,0]){
		donut(rDonut,hole_d);

		translate([l2,0]){
			donut(rDonut,hole_d);
		}
	}


}

// Centered in the center of the hole
// Radius of the circle
// Diameter of the hole
module donut(radius,hole_d){
	diam = radius*2;
	difference(){
		circle(d=diam);
		circle(d=hole_d);
	}

}
