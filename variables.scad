$fa=10;

materialLength=24*25.4;//in
materialWidth=6.5*25.4;//in

plateLength=215.9;
plateWidth=139.7;
thickness=15;
radDonut=10;
hole_diam=6.85;

baseLength = plateLength + radDonut*2;

topWidth = plateWidth - thickness*2;



h = (thickness); 
l = plateLength/2 + radDonut;
r = sqrt(h*h + l*l); // Length of arm from donut to donut
originalA = atan2(h,l);
a = 0; // Angle of elevation!
// Calculate displacement in x,z according to a
ztop = r*sin(a+originalA)-thickness;
xtop = plateLength/2  +radDonut - r*cos(a+originalA);
