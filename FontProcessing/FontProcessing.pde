import processing.svg.*;
PFont font;

String chars;

void setup(){
	size(500,500,SVG,"font.svg");
	background(255);

	chars = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZÁÉÍÓÚÜabcdefghijklmnñopqrstuvwxyzáéíóúü?!().,";


//	font = loadFont("SejoScript-42.vlw");
	font = createFont("SejoScript",42);

	int tSize = 42;
	textFont(font);
	textSize(tSize);

	float x,y;
	float h = tSize;
	float w;
	float tw;
	char c;

	x=0;
	y=0;
	int N_COLS = 36;

	for(int i=0; i<chars.length(); i++){
		c = chars.charAt(i);
		tw = textWidth(c);
		w = tw*1.05;
		fill(0);
		stroke(255,0,0);
		strokeWeight(0.01);
		rect(x,y,w,h);
	
		fill(255);
		noStroke();
		text(c,x + (w-tw)/2,y+tSize*0.85);

		x += w + 1;

		if(i%N_COLS == N_COLS-1){
			x = 0;
			y += h + 1;
		}

	}

	exit();

}

void draw(){

}
