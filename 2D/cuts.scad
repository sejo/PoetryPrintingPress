include <../variables.scad>
use <../Assembled/AssembledPress.scad>

//scale(1){
scale(1/25.4){ // Convert to In

// Cut 1
union(){
%square([materialLength,materialWidth]);

// Side of top (x2)
translate([0,topWidth+thickness]){
	rectPieceWithDonut(thickness,plateLength,plateLength/2);
	translate([plateLength+thickness,0]){
		rectPieceWithDonut(thickness,plateLength,plateLength/2);
	}
}

// Top plate
square([plateLength,topWidth]);




// Side of base
translate([plateLength+thickness,0]){
rectPieceWithDonut(thickness,baseLength,baseLength-radDonut);

translate([0,thickness*2 + radDonut*2]){
rectPieceWithDonut(thickness,baseLength,baseLength-radDonut);

translate([0,thickness*3])
	// Inner side of base 
	rectPiece(thickness,baseLength);
}





}


}

// Cut 2
*union(){
%square([materialLength,materialWidth]);

// Base plate
square([plateLength,plateWidth]);

translate([plateLength+thickness*2,0]){
// Base inner sect
rectPiece(thickness,baseLength);

// Legs (x2)
translate([0,thickness*3]){
#legWithThreeDonuts(r,r,thickness);
translate([0,thickness*3])
#legWithThreeDonuts(r,r,thickness);
}
}

}

}//End scale
