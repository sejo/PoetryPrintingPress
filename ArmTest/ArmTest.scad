
//linear_extrude(10){
    leg(20,5,30,40,10);
//}

module leg(diam,hole_size,l1,l2,h){
    hole_r = hole_size/2;
    union(){
        // First donut
        donut(diam,hole_size);
        
        // First arm
        translate([hole_r,-h/2,0]){
            square([l1-hole_size,h]);
        }       
        
        // Other two donuts
        translate([l1,0,0]){
            // Second donut
            donut(diam,hole_size);  
           
            //Second arm       
            translate([hole_r,-h/2,0]){
                square([l2-hole_size,h]);
            }  
          
          // Third donut  
            translate([l2,0,0]){
             donut(diam,hole_size);   
            }
        }

    }
}

module donut(diam,hole_size){
        difference(){
            circle(d=diam);
            circle(d=hole_size);
        }
}